<?php

namespace App\Http\Controllers\WebPages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

use App\Models\Assignments;
use App\Library\Validations;

class HomeController extends Controller
{
    public function __construct()
    {

    }

    public function viewPage(Request $request)
    {
        $season = $request->query('season');
        $season = isset($season) ? $request->query('season') : 2017;
        $divisions = Assignments::select('division')->where('season', $season)->distinct()->pluck('division');

        return view('pages.home')
            ->with([
                'divisions' => $divisions,
                'season' => $season
            ]);
    }

    public function getRegions(Request $request)
    {
        if(!Validations::division_getRegions($request->all())->fails())
        {
            $division = $request->query('division');
            $season = $request->query('season');
            return Assignments::select('current_region')->where([
                ['division',$division],
                ['season',$season]
            ])->distinct()->pluck('current_region');
        }

        return response('Error.',400);
    }

    public function filter(Request $request)
    {
        if(!Validations::divisionRegion_filter($request->all())->fails())
        {
            $season = $request->query('season');
            $division = $request->query('division');
            $region = $request->query('region');

            return DB::select('CALL filterStatistics(?, ?, ?)', [$season, $division, $region]);
        }

        return response('Error.',400);
    }
}
