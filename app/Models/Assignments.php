<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Assignments extends Model
{
    protected $table = "assignments";
    public $timestamps  = false;

    // public function broker()
    // {
    //     return $this->hasOne('App\Models\Common\Brokers','id','broker_id');
    // }
}
