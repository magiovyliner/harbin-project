<?php

namespace App\Library;

use Validator;

class Validations {

    public static function division_getRegions(array $data)
    {
        return Validator::make($data, [
            'division' => 'required|numeric|min:1|max:7',
            'season' => 'required|numeric|min:2017|max:2018'
        ]);
    }

    public static function divisionRegion_filter(array $data)
    {
        return Validator::make($data, [
            'division' => 'required|numeric|min:1|max:7',
            'region' => 'required|numeric|min:1|max:50',
            'season' => 'required|numeric|min:2017|max:2018'
        ]);
    }

}