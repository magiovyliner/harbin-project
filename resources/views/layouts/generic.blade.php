<!DOCTYPE html>
<!--[if IE 9 ]><html class='ie9'><![endif]-->
    <head>
        @include('includes.header')
    </head>

    <body class='bg-light'>
        
        @include('includes.navbar')
        
        <div class="wrapper">
            <div class='section'>
                @yield('content')
            </div>
            
            <footer class="footer text-white" style="background-color: black;">
                <div class="container">
                    <nav>
                        <ul>
                            <li>
                                <a href="/">
                                    Harbin Project
                                </a>
                            </li>
                            <li>
                                <a href="/">
                                    About Us
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="copyright">
                        ©
                        <script>
                            document.write(new Date().getFullYear())
                        </script> Harbin Project
                    </div>
                </div>
            </footer>

        </div>
        
        @include('includes.footer')
        
    </body>
</html>