<!DOCTYPE html>
<!--[if IE 9 ]><html class='ie9'><![endif]-->
    <head>
        @include('includes.header')
    </head>

    <body>
        
        @include('includes.navbar')
        
        <section id='main'>
            @include('includes.sidebar')
            @include('includes.watchlist')
            @yield('content')
        </section>
                
        @include('includes.footer')
        
    </body>
</html>