
@extends('layouts.generic')

@section('title','Home')

@section('css_includes')
   
    @parent

@endsection

@section('content')
<div id="home">
    <div class="container" id="home" style="background-color: #fff;">
        <img src="{{ asset('images/FBmainpage18-NEWNEW.png') }}" class="img-fluid" style="margin-top: -70px;"/>
        <div class="row mt-5">            
            <div class="col-md-12 text-center">
                <h2 class="title">Ohio High School Football</h2>
            </div>
        </div>  
        <div class="row pt-1 pb-1">
            <div class="col-md-12 text-center">
                 Latest update: <b class="text-success">10/28/2017 11:29:07 PM EDT</b>
            </div>
        </div>
        <span class="sexy_line"></span>
        <div class="row pt-1 pb-1">
            <div class="col-md-12 text-center">
                Please remember forfeits, incorrect game results, or schedule changes can have a<br>
                tremendous impact on the playoff status for many teams.<br>
                These statuses are <b>ALWAYS</b> unofficial, and <b>ONLY</b> intended to be used as a guide.<br>
            </div>
        </div>   
        <span class="sexy_line"></span>     
        <div class="row pt-1 pb-1">
            <div class="col-md-12 text-center">                
                View the <u><b>official</b></u> OHSAA rankings (after week 4) by <a target="__blank" href="http://www.ohsaa.org/sports/football">clicking here</a>.
            </div>
        </div>
        <span class="sexy_line"></span>
        <div class="row pt-1 pb-1">
            <div class="col-md-12 text-center">                
                Top 8 teams following week 10 qualify for playoffs
            </div>
        </div>        
        <div class="row mt-0">
            <div class="col-md-12 subheader">
                <span id='season'>{{ $season }}</span> <u>Unofficial</u> Rankings by Division and Region Filtering
            </div>
        </div>      
        <div class="row mt-0">
            <div class="col-md-12" style="justify-content: center">
                <div style="flex-direction: row;display: -ms-flexbox;display: flex; padding-left: 0;margin-bottom: 0;list-style: none;justify-content: center;">
                    <div class="dropdown text-black" style="margin-right:20px;">
                        <a href="#pablo" class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="division" style="background-color: black; cursor: pointer;">
                            @{{ division }}
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="division">
                            @foreach ($divisions as $division)
                                <a class="dropdown-item" href="#pablo" @click="getRegion({{ $division }})" >Division {{ $division }}</a>
                            @endforeach                            
                        </ul>
                    </div>
                    <div class="dropdown text-black" style="margin-right:20px;">
                        <a href="#pablo" class="btn btn-default dropdown-toggle disabled" data-toggle="dropdown" id="region" style="background-color: black; cursor: pointer;">
                            @{{ region }}
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="region" id="region_list" >
                            <a class="dropdown-item" href="#pablo" v-for='region in regions' @click="selectRegion(region)">Region @{{ region }}</a>
                        </ul>
                    </div>
                    <button class="btn btn-success disabled" id="filter" @click="filter">Filter</button>                    
                </div>
            </div>
        </div>
    </div>    
    <div class="row mt-1">            
        <div class="col-md-12 text-center">
            <filtered-data :results=results></filtered-data>
        </div>
    </div>
    <div class="row mt-3"></div>
    <div class="row mt-5">
        <div class="col-md-10 text-center justify-content-center" style='font-size: 11px; margin: auto;'>
            Disclaimer: These ratings are provided for informational purposes only. These ratings are computed using my implementation of the OHSAA playoff rating system outlined <a target="__blank" href="http://www.ohsaa.org/sports/football/computerrankingsinfo">here</a>, and do not reflect any personal opinions or bias regarding the relative strength or ability of any individual teams. By viewing these ratings, you agree that any misinformation provided is unintentional. Further, you also agree that the site owner and/or content writer cannot and will not be held responsible for any damages resulting from use of information published on this site. 
        </div>
    </div>
</div>
@endsection


@section('js_includes')
    
    @parent
    <script src="{{ asset('js/home.js') }}"></script>
@endsection