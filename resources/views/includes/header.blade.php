<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport">
@section('other_meta')
@show

<title>@yield('title') | Harbin Project</title>

<!--  faveicon -->
<link rel='shortcut icon' type='image/x-icon' href="{{ asset('./favicon.ico') }}" />

<!-- CSS -->
@section('css_includes')
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/now-ui-kit.css?v=1.1.0') }}">
<link rel="stylesheet" href="{{ asset('css/ext.css') }}">
@show