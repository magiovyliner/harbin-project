


@section('js_includes')
<script src="{{ asset('js/core/jquery.3.2.1.min.js') }}"></script>
<script src="{{ asset('js/core/popper.min.js') }}"></script>
<script src="{{ asset('js/core/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/now-ui-kit.js?v=1.1.0') }}"></script>
<script src="{{ asset('js/vue@2.4.2.js') }}"></script>
<script src="{{ asset('js/axios.min.js') }}"></script>
@show