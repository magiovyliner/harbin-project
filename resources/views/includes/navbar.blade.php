<header>
    <nav class="navbar navbar-expand-lg text-white pt-0 pb-0" color-on-scroll="400" style="background-color: black !important; margin-bottom: 0px">
        <div class="container">
            <div class="navbar-translate">
                <a class="navbar-brand" href="/" rel="tooltip" title="" data-placement="bottom" target="_blank">
                    Harbin Project
                </a>
            </div>
            <div class="collapse navbar-collapse justify-content-end" id="navigation">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="/">Home</a>
                    </li>
                    <div class="dropdown button-dropdown">
                        <a href="#pablo" class="dropdown-toggle" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false" style="text-transform: uppercase;font-size: 0.7142em;padding: 0.5rem 0.7rem;line-height: 1.625rem;">
                            Season {{ $season }} 
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-header">Seasons</a>
                            <a class="dropdown-item" href="/?season=2018">2018</a>
                            <a class="dropdown-item" href="/?season=2017">2017</a>
                        </div>
                    </div>
                    <div class="dropdown button-dropdown">
                        <a href="#pablo2" class="dropdown-toggle" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false" style="text-transform: uppercase;font-size: 0.7142em;padding: 0.5rem 0.7rem;line-height: 1.625rem;">
                            ScoreBoards
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-header">2018 ScoreBoards</a>
                            <a class="dropdown-item" href="#">Week 1</a>
                            <a class="dropdown-item" href="#">Week 2</a>
                            <a class="dropdown-item" href="#">Week 3</a>
                            <a class="dropdown-item" href="#">Week 4</a>
                            <a class="dropdown-item" href="#">Week 5</a>
                            <a class="dropdown-item" href="#">Week 6</a>
                            <a class="dropdown-item" href="#">Week 7</a>
                            <a class="dropdown-item" href="#">Week 8</a>
                            <a class="dropdown-item" href="#">Week 9</a>
                            <a class="dropdown-item" href="#">Week 10</a>
                            <a class="dropdown-item" href="#">Week 11</a>
                            <a class="dropdown-item" href="#">Week 12</a>
                            <a class="dropdown-item" href="#">Week 13</a>
                            <a class="dropdown-item" href="#">Week 14</a>
                            <a class="dropdown-item" href="#">Week 15</a>
                        </div>
                    </div>
                </ul>
            </div>
        </div>
    </nav>
</header>