
$(document).ready(function() {

    if($('#home')[0]){

        Vue.component('filtered-data', {
            template: "<div v-if='results[0]'>\
                            <h4>OHSAA {{ results.division }}, {{ results.region }}</h4>\
                            <table class='table table-bordered table-striped' style='font-size: 12px; width: 90%; margin: auto;'>\
                            <thead>\
                                <tr>\
                                    <th class='text-center' data-column-id='current_rank' data-type='numeric'>Current <br> Rank</th>\
                                    <th class='text-center' data-column-id='w_l'>W-L</th>\
                                    <th class='text-center' data-column-id='id_num'>ID #</th>\
                                    <th class='text-center' data-column-id='mailing_city'>Mailing City</th>\
                                    <th class='text-center' data-column-id='school'>School</th>\
                                    <th class='text-center' data-column-id='school'>Current <br> Average</th>\
                                    <th class='text-center' data-column-id='level_1'>Level 1</th>\
                                    <th class='text-center' data-column-id='level_2'>Level 2</th>\
                                    <th class='text-center' data-column-id='l2_divisor'>L2 Divisor</th>\
                                    <th class='text-center' data-column-id='status'>Playoff Status <br> (unofficial)</th>\
                                    <th class='text-center' data-column-id='maxavg_wo'>max Avg <br>(win out)</th>\
                                    <th class='text-center' data-column-id='minavg_wo'>min Avg <br>(win out)</th>\
                                    <th class='text-center' data-column-id='maxavg_lo'>max Avg <br>(lose out)</th>\
                                    <th class='text-center' data-column-id='minavg_lo'>min Avg <br>(lose out)</th>\
                                    <th class='text-center' data-column-id='l2_divisor_final'>L2 Divisor <br>(final)</th>\
                                </tr>\
                            </thead>\
                            <tbody>\
                                <tr v-for='(result, index) in results'>\
                                    <td style='padding: 0.25rem !important;'>{{ result.rank }}</td>\
                                    <td style='padding: 0.25rem !important;'>{{ result.w_l }}</td>\
                                    <td style='padding: 0.25rem !important;'>{{ result.school_id }}</td>\
                                    <td style='padding: 0.25rem !important;'>{{ result.city }}</td>\
                                    <td style='padding: 0.25rem !important;'>{{ result.school_name }}</td>\
                                    <td style='padding: 0.25rem !important;'>{{ result.current_average }}</td>\
                                    <td style='padding: 0.25rem !important;'>{{ convertDecimals(result.level_1) }}</td>\
                                    <td style='padding: 0.25rem !important;'>{{ convertDecimals(result.level_2) }}</td>\
                                    <td style='padding: 0.25rem !important;'>{{ result.l2_divisor }}</td>\
                                    <td :class='status(result.status)' style='padding: 0.25rem !important;font-size: 10px;'>{{ result.status }}</td>\
                                    <td style='padding: 0.25rem !important;'>{{ result.maxavg_wo }}</td>\
                                    <td style='padding: 0.25rem !important;'>{{ result.minavg_wo }}</td>\
                                    <td style='padding: 0.25rem !important;'>{{ result.maxavg_lo }}</td>\
                                    <td style='padding: 0.25rem !important;'>{{ result.minavg_lo }}</td>\
                                    <td style='padding: 0.25rem !important;'>{{ result.l2_divisor_final }}</td>\
                                </tr>\
                            </tbody>\
                        </table>\
                    </div>\
                    <div class='f-700 text-center mt-5' v-else> Select first Division and Region </div>\
                    ",
            props: ['results'],
            methods: {
                convertDecimals: function(val){
                    // numberOfDecimal = (Number(val) < 10000) ? 2 : 0;
                    numberOfDecimal = 2;
                    return Number(val).toLocaleString('en-US', { style: 'decimal', minimumFractionDigits: numberOfDecimal, maximumFractionDigits: numberOfDecimal });
                },
                status: function(val){
                    if (val.includes('eliminated'))
                        return 'text-danger';
                }
            }
        });

        var homepage = new Vue({
            el: '#home',
            data: function(){
                return{
                    results: [],
                    regions: [],
                    division: 'Select Division',
                    region: 'Select Region',
                    season: 0
                }
            },
            mounted: function(){
                this.season = $('#season').text();
            },
            methods: {
                renderDataTable: function(){
                    $(".data-table-basic").bootgrid({
                        css: {
                            icon: 'zmdi icon',
                            iconColumns: 'zmdi-view-module',
                            iconDown: 'zmdi-sort-amount-desc',
                            iconRefresh: 'zmdi-refresh',
                            iconUp: 'zmdi-sort-amount-asc'
                        },
                        labels: {
                            infos: "{{ctx.start}} to {{ctx.end}} of {{ctx.total}}"
                        },
                        formatters: {
                            "symbol": function(column, row)
                            {
                                if (fundShowResults.watchlist_symbols.includes(row.symbol))
                                    return "<a class='being-watched-row' href='company-profile/" + row.symbol + "'>" + row.symbol + "</a>";
                                return "<a href='company-profile/" + row.symbol + "'>" + row.symbol + "</a>";
                            },
                            "name": function(column, row)
                            {
                                return "<a href='company-profile/" + row.symbol + "'>" + row.name + "</a>";
                            },
                        },
                        columnSelection: false,
                        padding: 2,
                        navigation: 3,            
                        paginationItem: 3,
                        sorting: false,
                        caseSensitive: false
                    }).on("loaded.rs.jquery.bootgrid", function()
                    {  $(".being-watched-row").parent().parent().find("a").addClass("being-watched-row");
                    $(".being-watched-row").parent().parent().find("td").addClass("being-watched-row"); });
                },
                getRegion: function(division){
                    if(division !== undefined){
                        this.division = 'Division ' + division;
                        this.region = 'Select Region';
                        $('a#region').removeClass('disabled');
                        $('button#filter').addClass('disabled');
                        axios.get('/regions?division=' + division + '&season=' + this.season).then(response => {                            
                            this.regions = response.data;
                        }).catch(error => {

                        });
                    }
                },
                selectRegion: function(region){
                    if(region !== undefined){
                        this.region = 'Region ' + region;
                        $('button#filter').removeClass('disabled');
                    }
                },
                filter: function(){
                    division = this.division.replace('Division ', '');
                    region = this.region.replace('Region ', '');
                    axios.get('/filter?season=' + this.season + '&division=' + division + '&region=' + region).then(response => {
                        this.results = response.data;
                        this.results.division = this.division;
                        this.results.region = this.region;
                    });
                }
            }
        });

    }


});
